"""
    Caching data
"""

import logging, time
from google.appengine.api import memcache
from google.appengine.ext import db


def get(key, object_id):
    """
        call to this function and routed to subroutines based on key. Takes a key str
        returns a cache value
    """

    if key == 'single_wiki':
        return check_cache(key, object_id)

    return None


def check_cache(key, object_id):

    # lookup the cache
    value = memcache.get(key)

    # check for a Wiki id to see if this is the same request for a Wiki
    prev_lookup = None
    if value:
        prev_lookup = value[2]

    # hit the db query if no cache or requesting a different Wiki id
    if prev_lookup is None or (prev_lookup != object_id):
        logging.error("DB QUERY:" + str(object_id))
        wkey = str(db.Key.from_path('WikiPage', int(object_id), parent=wiki_key()))
        wiki = db.get(wkey)

        value = (wiki, time.time(), object_id)
        memcache.set(key, value)

    return value


def single_wiki(topic):

    # lookup the cache
    value = memcache.get(key)

    # check for a Wiki id to see if this is the same request for a Wiki
    cache_wiki_id = None
    if value:
        cache_wiki_id = value[2]

    # hit the db query if no cache or requesting a different Wiki id
    if cache_wiki_id is None or (cache_wiki_id != wiki_id):
        logging.error("DB QUERY")
        Wiki_key = str(
            db.Key.from_path('Wiki', int(wiki_id), parent=wiki_key()))
        Wiki = db.get(Wiki_key)

        value = (Wiki, time.time(), wiki_id)
        memcache.set(key, value)

    return value


def wiki_key(name='default'):

    """
      value of the wiki's parent for Google app data store
    """

    return db.Key.from_path('wikis', name)

wiki_ids = {}

def get_wiki_id(subject):
    """
        gets the wiki obj id associated with that subject
    """
    pageQuery = db.GqlQuery("SELECT * FROM WikiPage WHERE subject = :1", subject)
    wiki = pageQuery.get()
    if wiki:
        result = wiki.key().id()
        return result

    return None

    # wiki_ids.get(subject)

def set_wiki_id(subject, wiki_id):
    """
        sets the wiki subject to the wiki_id. returns void
    """

    wiki_ids[subject] = wiki_id




