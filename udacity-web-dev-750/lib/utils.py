"""
  hashing, cookies, salting
"""

import random
import string
import hashlib
import hmac
import re


# hashing

def make_pw_hash(name, pw, salt=None):
    """
      returns a hashed password of the format: 
        HASH(name + pw + salt)|salt
      uses sha256
    """

    if not salt:
        salt = make_salt()

    h = hashlib.sha256(name + pw + salt).hexdigest()

    return '%s|%s' % (h, salt)


def make_hash(value, salt=None):
    """
      returns a hashed value of the format: 
        HASH(value + salt),salt
      uses sha256
    """

    if not salt:
        salt = make_salt()

    h = hashlib.sha256(value + salt).hexdigest()

    return '{}|{}'.format(h, salt)


def make_salt():
    """
      returns a string of 5 random letters.
    """
    salt = ''.join(random.choice(string.letters) for i in xrange(5))

    return salt


# hash functions
SECRET = 'imsosecret'


def verify_authenticity(field):
    field_cookie_str = self.request.cookies.get(field)

    # verify cookie authenticity
    if field_cookie_str:
        cookie_val = check_secure_val(field_cookie_str)
        if cookie_val:
            # visits = int(cookie_val)
            field = cookie_val


def hash_str(s):
    """
    # Implement the hash_str function to use HMAC and our SECRET instead of md5
    """
    return hmac.new(SECRET, s).hexdigest()


def make_secure_val(s):
    """
      Implement the function make_secure_val, which takes a string and returns a 
      string of the format: 
      s,HASH
    """
    return '{}|{}'.format(s, hash_str(s))


def check_secure_val(h):
    """
      Implement the function check_secure_val, which takes a string of the format 
      s|HASH
      and returns s if hash_str(s) == HASH, otherwise None 
    """
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val


def valid_cookie_val(value, h):
    """
       returns True if a user's password matches its hash.
    """
    salt = h.split(',')[1]
    result = make_pw_hash(name, pw, salt) == h

    return result


def valid_pw(name, pw, h):
    """
       returns True if a user's password matches its hash.
    """
    salt = h.split('|')[1]
    result = make_pw_hash(name, pw, salt) == h

    return result
