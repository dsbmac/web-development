"""
  Handler for Ascii art

"""


# base handler for basic functionality inheritance
from handlers.base import AppHandler
from xml.dom import minidom
import urllib2, time
import logging

# for google data storage
from google.appengine.ext import db
from google.appengine.api import memcache


class Art(db.Model):
    title = db.StringProperty(required=True)
    art = db.TextProperty(required=True)
    coordinates = db.GeoPtProperty(required=False)
    created = db.DateTimeProperty(auto_now_add=True)


class MainPage(AppHandler):
    """
      front page handler for ascii art app
    """

    def get(self):

        # debug line
        # self.write(repr(get_coords(self.request.remote_addr)))

        return self.render_front()

    def post(self):
        title = self.request.get('title')
        art = self.request.get('art')

        if title and art:
            a = Art(title=title, art=art)

            # lookup the user's coordinates from their ip
            coordinates = get_coords(self.request.remote_addr)

            # if we have coordinates, add them to the Art
            if coordinates:
                a.coordinates = coordinates

            a.put()

            # delay so the query gets the new write
            time.sleep(0.1)

            # rerun the art query with cache update
            top_arts(True)
            self.render_front()

        else:
            error = "We need a title and art for a valid submission"
            self.render_front(title=title, art=art, error=error)

    def render_front(self, arts=None, title='', art='', error=''):

        # get art from cache
        arts = top_arts()

        # find which arts have coords
        points = filter(None, (a.coordinates for a in arts))

        # if we have coords then get the google map url
        if points: 
            img_url = gmaps_img(points)

        # display the map

        self.render("/unit3/ascii_art.html",
         arts=arts, img_url=img_url, error=error, title=title, art=art)



def top_arts(update=False):
    """
      takes no args and returns a list of Art objects from a db 
      query
    """

    key = 'top'

    # lookup the cache
    arts = memcache.get(key)

    # hit the db query if no cache or an update is requested 
    if update or arts is None :
        logging.error("DB QUERY")
        arts = db.GqlQuery("SELECT * FROM Art ORDER BY created DESC")

        # duplicate arts to avoid multiple db hits
        arts = list(arts)
        memcache.set(key, arts)
    
    return arts


xml = """<HostipLookupResultSet xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0.1" xsi:noNamespaceSchemaLocation="http://www.hostip.info/api/hostip-1.0.1.xsd">
           <gml:description>This is the Hostip Lookup Service</gml:description>
           <gml:name>hostip</gml:name>
           <gml:boundedBy>
             <gml:Null>inapplicable</gml:Null>
           </gml:boundedBy>
           <gml:featureMember>
             <Hostip>
               <ip>12.215.42.19</ip>
               <gml:name>Aurora, TX</gml:name>
               <countryName>UNITED STATES</countryName>
               <countryAbbrev>US</countryAbbrev>
               <!-- Co-ordinates are available as lng,lat -->
               <ipLocation>
                 <gml:pointProperty>
                   <gml:Point srsName="http://www.opengis.net/gml/srs/epsg.xml#4326">
                     <gml:coordinates>-97.5159,33.0582</gml:coordinates>
                   </gml:Point>
                 </gml:pointProperty>
               </ipLocation>
             </Hostip>
           </gml:featureMember>
        </HostipLookupResultSet>"""

IP_URL = "http://api.hostip.info/?ip="


def get_coords(xml):
    """
      takes in an xml string and returns a GeoPt of (lat, lon)
      if there are coordinates in the xml.
      Remember that you should use minidom to do this.
      Also, notice that the coordinates in the xml string are in the format:
      (lon,lat), so you will have to switch them around.
    """

    # this part is for debugging and bypasses the xml argument
    ip = '4.2.2.2'  # big name server
    url = IP_URL + ip
    content = None

    try:
        content = urllib2.urlopen(url).read()

    except URLError:
        return

    if content:

        # build the dom
        dom = minidom.parseString(content)
        node = dom.documentElement
        coords_lst = node.getElementsByTagName("gml:coordinates")
        coords_str = coords_lst[0].childNodes[0].nodeValue
        lon, lat = coords_str.split(',')

        return db.GeoPt(lat, lon)


GMAPS_URL = "http://maps.googleapis.com/maps/api/staticmap?size=380x263&sensor=false&"


def gmaps_img(points):
    """
      implement the function gmaps_img(points) that returns the google maps image
      url for a map with the points passed in. A example valid response looks
      like this:

      http://maps.googleapis.com/maps/api/staticmap?size=380x263&sensor=false&markers=1,2&markers=3,4

      Note that you should be able to get the first and second part of an individual Point p with
      p.lat and p.lon, respectively, based on the above code. For example, points[0].lat would
      return 1, while points[2].lon would return 6.
    """

    markers = '&'.join('markers={},{}'.format(pt.lat, pt.lon) for pt in points)
    result = GMAPS_URL + markers

    return result







