"""
  Handler for wiki
"""

# base handler for basic functionality inheritance
from handlers.base import AppHandler
from lib import utils
from lib import cache
from lib.models import User
from handlers import data
import jinja2, logging, time, re
from main import template_dir
from google.appengine.ext import db  # for google data storage


# jinja template
jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(template_dir))


class WikiPage(db.Model, AppHandler):

    """
      WikiPage object for the Google data storage db  
    """

    jinja = jinja_environment
    subject = db.StringProperty(required=True)
    content = db.TextProperty(required=False)
    created = db.DateTimeProperty(auto_now_add=True)
    last_modified = db.DateTimeProperty(auto_now=True)


    def render(self):
        self._render_text = self.content.replace('\n', '<br>')
        return self.render_str("/wiki/wiki_page.html", wiki=self)


    # no json for the moment
    # def render_json(self):
    #     json_form = '{{"content": "{0}", "created": "{1}", "last_modified": "{2}", "subject": "{3}"}}'
    #     return json_form.format(self.content,
    #                             self.created.strftime("%b %d, %Y"),
    #                             self.last_modified.strftime("%b %d, %Y"),
    #                             self.subject)



class Page(AppHandler):

    def get(self, subject):
        logging.error("get Page: " + subject)

        # get the db object id of the subject if there is one
        link = self.request.url
        wiki_id = cache.get_wiki_id(subject)
        logging.error("wiki_id: " + str(wiki_id))

        if wiki_id:

            logging.error("DB QUERY:" + str(wiki_id))
            wkey = str(db.Key.from_path('WikiPage', int(wiki_id), parent=wiki_key()))
            wiki = db.get(wkey)

            #this if to lookup cache
            # key = 'single_wiki'
            # cached_data = cache.get(key, wiki_id)
            # wiki = cached_data[0]
        
            if not wiki:
                self.error(404)
                return

            # time_last_update = cached_data[1]
            # time_since = round(time.time() - time_last_update)
            time_since = 0

            self.render("/wiki/wiki_page.html", wiki=wiki, time_since=time_since)

        # no subject was found in the subject/id dict            
        # so go to edit page
        else:
            self.redirect('/wiki/_edit/{}'.format(subject))


class EditPage(AppHandler): 

    def get(self, subject):
        wiki_content = get_wiki_content(subject)
        logging.error("wiki_content: " + wiki_content)

        self.render("/wiki/edit_page.html", wiki_content=wiki_content, time_since=0)

    def post(self, subject):
        content = self.request.get('wiki_content')
        wiki_id = cache.get_wiki_id(subject)

        if wiki_id and content:
            self.update_wiki(wiki_id, subject, content)
       
        else:
            self.create_wiki(subject, content)

        # else:
        #     error = "Please include a subject and content!"
        #     self.render("/wiki/new_Wiki.html", subject=subject,
        #                 content=content, error=error)


    def update_wiki(self, wiki_id, subject, content):
        logging.error("update_wiki: " + str(wiki_id))
        wkey = str(db.Key.from_path('WikiPage', int(wiki_id), parent=wiki_key()))
        wiki = db.get(wkey)
        wiki.content = content

        self.redirect('/wiki/{}'.format(subject))


    def create_wiki(self, subject, content):
        """
          helper to create a new WikiPage returns void.
        """
        logging.error("create_wiki...")
        w = WikiPage(parent=wiki_key(), subject=subject, content=content)
        w.put()
        wiki_id = w.key().id()
        logging.error("creating wiki_id: " + str(wiki_id))

        cache.set_wiki_id(subject, wiki_id)

        # delay so the query gets the new write
        time.sleep(2)

        # update the cache
        # top_Wikis(True)

        self.redirect('/wiki/{}'.format(subject))


class Welcome(AppHandler):

    def get(self):

            # get the user id from cookie
        cookie_str = self.request.cookies.get('user_id')

        # authenticate user id hash
        # if cookie_str:
        # cookie_val = check_secure_val(visits_cookie_str)
        # if cookie_val:

        # if data.valid_username(username):
        if cookie_str:
            user_id = cookie_str.split('|')[0]
            usr = User.get_by_id(int(user_id))
            username = usr.username
            self.render('welcome.html', username=username)
        else:
            self.redirect('/wiki/signup')


def render_wiki(response, wiki):
    response.out.write('<b>' + wiki.subject + '</b><br>')
    response.out.write(wiki.content)


def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)


def wiki_key(name='default'):
    """
      value of the wiki's parent for Google app data store
    """

    return db.Key.from_path('wikis', name)


def get_wiki_content(subject):
    """
        takes the subject string and returns the string content from the database
    """
    logging.error("get_wiki_content...")

    wiki_id = cache.get_wiki_id(subject)
    logging.error("wiki_id:" + str(wiki_id))

    result = ''
    if wiki_id:
        logging.error("wiki_id found: " + subject)
        wkey = str(db.Key.from_path('WikiPage', int(wiki_id), parent=wiki_key()))
        wiki = db.get(wkey)

        # TODO return the content from db
        result = wiki.content

    else:
        logging.error("wiki_id not found: " + subject)


    return result


def top_Wikis(update=False):
    """
      takes no args and returns a tuple (Wikis, time)
    """

    key = 'top_Wikis'

    # lookup the cache
    value = memcache.get(key)

    # hit the db query if no cache or an update is requested
    if update or value is None:
        logging.error("DB QUERY")
        Wikis = db.GqlQuery("SELECT * FROM Wiki ORDER BY created DESC")

        # duplicate Wikis to avoid multiple db hits
        Wikis = list(Wikis)
        value = (Wikis, time.time())
        memcache.set(key, value)

    return value

class Flush(AppHandler):
  """
    flushes the cache
  """

  def get(self):
    memcache.flush_all()
    self.redirect('/wiki')


class Signup(AppHandler):
    """
      user registration signup
    """

    def get(self):
        self.render("signup_form.html")


    def Wiki(self):

        # extract input values
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        have_error, params = valid_signup(username, password, verify, email)

        if have_error:
            self.render('signup_form.html', **params)
        else:

            # this is a valid entry so we create a db object

            # hash the username and password
            pwh = utils.make_pw_hash(username, password)
            new_user = User(username=username, email=email, pw_hash=pwh)
            new_user.put()
            user_id = str(new_user.key().id())

            # set the cookie
            hash_val = utils.make_hash(user_id)
            self.response.headers.add_header('Set-Cookie', 
                                         'user_id={}|{}'.format(user_id, hash_val))

            # send them to a welcome page
            self.redirect('/wiki/welcome')


# validations
def valid_signup(username, password, verify, email):
  """
    validate user signup info
  """

  have_error = False
  params = dict(username = username, email = email)

  if not valid_username(username):
      params['error_username'] = "That's not a valid username."
      have_error = True

  if not valid_password(password):
      params['error_password'] = "That wasn't a valid password."
      have_error = True
  
  elif password != verify:
      params['error_verify'] = "Your passwords didn't match."
      have_error = True

  if not valid_email(email):
      params['error_email'] = "That's not a valid email."
      have_error = True

  return have_error, params


USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)


PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)


EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)