"""
  Handler for Blog
"""

# base handler for basic functionality inheritance
from handlers.base import AppHandler
from lib import utils
from lib.models import User
import jinja2
import logging
import time
import re
from main import template_dir
from google.appengine.ext import db  # for google data storage
from google.appengine.api import memcache


# jinja template
jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(template_dir))


class Post(db.Model, AppHandler):

    """
      Post object for the Google data storage db  
    """

    jinja = jinja_environment
    subject = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    last_modified = db.DateTimeProperty(auto_now=True)


    def render(self):
        self._render_text = self.content.replace('\n', '<br>')
        return self.render_str("/blog/post.html", p=self)


    def render_json(self):
        json_form = '{{"content": "{0}", "created": "{1}", "last_modified": "{2}", "subject": "{3}"}}'
        return json_form.format(self.content,
                                self.created.strftime("%b %d, %Y"),
                                self.last_modified.strftime("%b %d, %Y"),
                                self.subject)


class Front(AppHandler):

    def get(self):
        cache = top_posts()
        posts = cache[0]
        time_last_update = cache[1]
        time_since = round(time.time() - time_last_update)
        self.render('/blog/front.html', posts=posts, time_since=time_since)


class PostPage(AppHandler):

    def get(self, post_id):
        cache = single_post(post_id)
        post = cache[0]

        if not post:
            self.error(404)
            return

        time_last_update = cache[1]
        time_since = round(time.time() - time_last_update)

        self.render("/blog/permalink.html", post=post, time_since=time_since)


class NewPost(AppHandler):

    def get(self):
        self.render("/blog/new_post.html")

    def post(self):
        subject = self.request.get('subject')
        content = self.request.get('content')

        if subject and content:
            self.create_post(subject, content)

        else:
            error = "Please include a subject and content!"
            self.render("/blog/new_post.html", subject=subject,
                        content=content, error=error)


    def create_post(self, subject, content):
        """
          helper to create a new post returns void.
        """

        p = Post(parent=blog_key(), subject=subject, content=content)
        p.put()

        # delay so the query gets the new write
        time.sleep(0.1)

        # update the cache
        top_posts(True)

        self.redirect('/blog/{}'.format(p.key().id()))


class Welcome(AppHandler):

    def get(self):

        # get the user id from cookie
        cookie_str = self.request.cookies.get('user_id')
        logging.info(cookie_str)
        
        # authenticate user id hash
        # if cookie_str:
        # cookie_val = check_secure_val(visits_cookie_str)
        # if cookie_val:

        # if data.valid_username(username):
        if cookie_str:
            user_id = cookie_str.split('|')[0]
            usr = User.get_by_id(int(user_id))
            username = usr.username
            self.render('welcome.html', username=username)
        else:
            self.redirect('/blog/signup')


def render_post(response, post):
    response.out.write('<b>' + post.subject + '</b><br>')
    response.out.write(post.content)


def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)


def blog_key(name='default'):
    """
      value of the blog's parent for Google app data store
    """
    return db.Key.from_path('blogs', name)


def single_post(post_id):
    key = 'single_post'

    # lookup the cache
    value = memcache.get(key)

    # check for a post id to see if this is the same request for a post
    cache_post_id = None
    if value:
        cache_post_id = value[2]

    # hit the db query if no cache or requesting a different post id
    if cache_post_id is None or (cache_post_id != post_id):
        logging.error("DB QUERY")
        post_key = str(
            db.Key.from_path('Post', int(post_id), parent=blog_key()))
        post = db.get(post_key)

        value = (post, time.time(), post_id)
        memcache.set(key, value)

    return value


def top_posts(update=False):
    """
      takes no args and returns a tuple (posts, time)
    """

    key = 'top_posts'

    # lookup the cache
    value = memcache.get(key)

    # hit the db query if no cache or an update is requested
    if update or value is None:
        logging.error("DB QUERY")
        posts = db.GqlQuery("SELECT * FROM Post ORDER BY created DESC")

        # duplicate posts to avoid multiple db hits
        posts = list(posts)
        value = (posts, time.time())
        memcache.set(key, value)

    return value


class Flush(AppHandler):

    """
      flushes the cache
    """

    def get(self):
        memcache.flush_all()
        self.redirect('/blog')


class Signup(AppHandler):

    """
      user registration signup
    """

    def get(self):
        self.render("signup_form.html")

    def post(self):

        # extract input values
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        have_error, params = valid_signup(username, password, verify, email)

        if have_error:
            self.render('signup_form.html', **params)
        else:

            # this is a valid entry so we create a db object

            # hash the username and password
            pwh = utils.make_pw_hash(username, password)
            new_user = User(username=username, email=email, pw_hash=pwh)
            new_user.put()
            user_id = str(new_user.key().id())

            # set the cookie
            hash_val = utils.make_hash(user_id)
            self.response.headers.add_header('Set-Cookie',
                                             'user_id={}|{}'.format(user_id, hash_val))

            # send them to a welcome page
            self.redirect('/blog/welcome')


# validations
def valid_signup(username, password, verify, email):
    """
      validate user signup info
    """

    have_error = False
    params = dict(username=username, email=email)

    if not valid_username(username):
        params['error_username'] = "That's not a valid username."
        have_error = True

    if not valid_password(password):
        params['error_password'] = "That wasn't a valid password."
        have_error = True

    elif password != verify:
        params['error_verify'] = "Your passwords didn't match."
        have_error = True

    if not valid_email(email):
        params['error_email'] = "That's not a valid email."
        have_error = True

    return have_error, params


USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")


def valid_username(username):
    return username and USER_RE.match(username)


PASS_RE = re.compile(r"^.{3,20}$")


def valid_password(password):
    return password and PASS_RE.match(password)


EMAIL_RE = re.compile(r'^[\S]+@[\S]+\.[\S]+$')


def valid_email(email):
    return not email or EMAIL_RE.match(email)
