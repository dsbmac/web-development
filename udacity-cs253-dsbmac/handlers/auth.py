"""
    handles authentication and cookies
"""


# base handler for basic functionality inheritance
from handlers.base import AppHandler
from google.appengine.ext import db
from lib import utils
from lib.models import User
import logging
import Cookie
import re


class Signup(AppHandler):

    """
      user registration signup
    """

    def get(self):
        self.render("signup_form.html")

    def post(self):

        # extract input values
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        have_error, params = valid_signup(username, password, verify, email)

        if have_error:
            self.render('signup_form.html', **params)
        else:

            # this is a valid entry so we create a db object

            # hash the username and password
            pwh = utils.make_pw_hash(username, password)
            new_user = User(username=username, email=email, pw_hash=pwh)
            new_user.put()
            user_id = str(new_user.key().id())

            # set the cookie
            hash_val = utils.make_hash(user_id)
            self.response.headers.add_header('Set-Cookie',
                                             'user_id={}|{}'.format(user_id, hash_val))

            # send them to a welcome page depending on which app
            app_name = extract_app_name(self.request.path)
            self.redirect('/{}/welcome'.format(app_name))


# validations
def valid_signup(username, password, verify, email):
    """
      validate user signup info
    """

    have_error = False
    params = dict(username=username, email=email)

    if not valid_username(username):
        params['error_username'] = "That's not a valid username."
        have_error = True

    if not valid_password(password):
        params['error_password'] = "That wasn't a valid password."
        have_error = True

    elif password != verify:
        params['error_verify'] = "Your passwords didn't match."
        have_error = True

    if not valid_email(email):
        params['error_email'] = "That's not a valid email."
        have_error = True

    return have_error, params


USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")


def valid_username(username):
    return username and USER_RE.match(username)


PASS_RE = re.compile(r"^.{3,20}$")


def valid_password(password):
    return password and PASS_RE.match(password)


EMAIL_RE = re.compile(r'^[\S]+@[\S]+\.[\S]+$')


def valid_email(email):
    return not email or EMAIL_RE.match(email)


# Login

class Login(AppHandler):

    """
    Handler for login 

    """

    def get(self):
        self.render("login_form.html")

    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')

        params = dict(username=username, password=password)

        # validate user info
        have_error, params, user = valid_login(username, password, params)

        if have_error:
            self.render('login_form.html', **params)

        else:

            # this is a valid entry so we set the cookie
            user_id = str(user.key().id())

            # set the cookie
            hash_val = utils.make_hash(user_id)
            self.response.headers.add_header('Set-Cookie',
                                             'user_id={}|{}'.format(user_id, hash_val))

            # send them to a welcome page according to the app
            app_name = extract_app_name(self.request.path)
            self.redirect('/{}/welcome'.format(app_name))



class Logout(AppHandler):

    def get(self):
        self.response.delete_cookie('user_id')

        # send them to a signup page
        self.redirect('/blog/signup')


def valid_login(username, password, params):
  """
    validates user login
  """
  have_error = False

  # find the user in the db
  query_str = "select * from User where username = '{}'".format(username)
  user = db.GqlQuery(query_str)
  #usr = utils.User.get_by_id(int(user_id))

  a = user.get()

  if not a:
    # no user found in db
    have_error = True
  
  else:
    # check hash password
    pwh = a.pw_hash
    logging.info('pwh: {}'.format(pwh))
    if not utils.valid_pw(username, password, pwh):
      have_error = True
  
  if have_error:
    params['error_login'] = "Invalid login."

  return have_error, params, a

def extract_app_name(path):
    """
        takes a string path and returns a string of the app name
        ex. extract_app_name('/wiki/signup')
        --> 'wiki'
    """
    found = None
    m = re.search('/([^a-c6]+)/', path)
    if m:
        found = m.group(1)

    return found

