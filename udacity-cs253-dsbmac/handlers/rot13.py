"""
  Handler for Rot13 page
"""

# base handler for basic functionality inheritance
from handlers.base import AppHandler


class Rot13(AppHandler):
    def get(self):
        self.render('rot13_form.html')

    def post(self):
        rot13 = ''
        text = self.request.get('text')
        if text:
            rot13 = text.encode('rot13')

        self.render('rot13_form.html', text = rot13)
