"""
  API for the blog. Outputs to json with .json ending on the permalink or front
  page
"""

import json
from handlers.base import AppHandler
from google.appengine.ext import db
from handlers.blog import blog_key


class Front(AppHandler):

    def get(self):
        self.response.headers["Content-Type"] = "application/json"

        # retrieve all posts
        posts = db.GqlQuery(
            "select * from Post order by created desc limit 10")

        # convert to string so that you can use .join in the html template
        posts_json = [p.render_json()  for p in posts]
        self.render('/blog/front_json.html', posts=posts_json)


class SinglePost(AppHandler):

    """docstring for SinglePost"""

    def get(self, post_id):
        self.response.headers["Content-Type"] = "application/json"

        # retrieve post
        key = db.Key.from_path('Post', int(post_id), parent=blog_key())
        post = db.get(key)

        if not post:
            self.error(404)
            return

        self.render("/blog/permalink_json.html", post=post)
