"""
  Handler for login 

"""

# base handler for basic functionality inheritance
from handlers.base import AppHandler
from google.appengine.ext import db
import logging, Cookie


class Login(AppHandler):
  """docstring for Login"""

  def get(self):
    self.render("login_form.html")


  def post(self):
    have_error = False
    username = self.request.get('username')
    password = self.request.get('password')

    params = dict(username=username, password=password)

    # validate user info
    have_error, params, user = valid_login(username, password, params)

    if have_error:
        self.render('login_form.html', **params)
    else:

        # this is a valid entry so we set the cookie
        user_id = str(user.key().id())

        # set the cookie
        hash_val = data.make_hash(user_id)
        self.response.headers.add_header('Set-Cookie', 
                                    'user_id={}|{}'.format(user_id, hash_val))

        # send them to a welcome page
        self.redirect('/blog/welcome')

class Logout(AppHandler):
  def get(self):
    self.response.delete_cookie('user_id' )

    # send them to a signup page
    self.redirect('/blog/signup')



def valid_login(username, password, params):
  """
    validates user login
  """
  have_error = False

  # find the user in the db
  query_str = "select * from User where username = '{}'".format(username)
  user = db.GqlQuery(query_str)
  #usr = data.User.get_by_id(int(user_id))

  a = user.get()

  if not a:
    # no user found in db
    have_error = True
  
  else:
    # check hash password
    pwh = a.pw_hash
    logging.info('pwh: {}'.format(pwh))
    if not data.valid_pw(username, password, pwh):
      have_error = True
  
  if have_error:
    params['error_login'] = "Invalid login."

  return have_error, params, a