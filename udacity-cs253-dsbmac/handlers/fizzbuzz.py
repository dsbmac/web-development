from handlers.base import AppHandler

class FizzBuzz(AppHandler):
    def get(self):
        n = 20
        self.render('fizzbuzz.html', n=n)