from handlers.base import AppHandler


class ShoppingList(AppHandler):
  """
    Handler for shopping list in lesson 2a.
  """

  def get(self):
    items = self.request.get_all("food")
    self.render('shopping_list.html', items=items)

