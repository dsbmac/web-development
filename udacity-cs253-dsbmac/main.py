import os
# from string import letters

# to enable special routing creation capability
from webapp2 import WSGIApplication, Route

# setup the template environment
# Set useful fields
root_dir = os.path.dirname(__file__)
template_dir = os.path.join(root_dir, 'templates')

PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'
routes = [
    Route(r'/rot13', handler='handlers.rot13.Rot13', name='rot13'),
    Route(r'/asciiart', handler='handlers.ascii.MainPage', name='ascii'),
    Route(r'/shopping', handler='handlers.shopping.ShoppingList',
           name='shopping'),
    Route(r'/fizzbuzz', handler='handlers.fizzbuzz.FizzBuzz', name='fizzbuzz'),
    # blog
    Route(r'/blog', handler='handlers.blog.Front', name='front'),
    Route(r'/blog/newpost', handler='handlers.blog.NewPost', name='newpost'),
    Route(r'/blog/<post_id:\d+>', handler='handlers.blog.PostPage', 
          name='blog_post'),
    Route(r'/blog/signup', handler='handlers.blog.Signup', name='signup'),
    Route(r'/blog/welcome', handler='handlers.blog.Welcome', name='welcome'),
    Route(r'/blog/login', handler='handlers.auth.Login', name='login'),
    Route(r'/blog/logout', handler='handlers.auth.Logout', name='logout'),
    # json
    Route(r'/blog/.json', handler='handlers.api.Front', name='front_json'),
    Route(r'/blog/<post_id:\d+>.json', handler='handlers.api.SinglePost', 
          name='json_post'),
    Route(r'/blog/flush', handler='handlers.blog.Flush', name='flush_cache'),

    # wiki
    # leave the signup and logout at the top to raise priority
    Route(r'/wiki/signup', handler='handlers.auth.Signup', name='signup'),
    Route(r'/wiki/login', handler='handlers.auth.Login', name='login'),
    Route(r'/wiki/logout', handler='handlers.auth.Logout', name='logout'),
    Route(r'/wiki/welcome', handler='handlers.wiki.Welcome', name='welcome'),

    # lower priority
    Route(r'/wiki/<subject:[a-zA-Z0-9_-]+>', handler='handlers.wiki.Page', 
          name='wiki_page'),
    Route(r'/wiki/_edit/<subject:[a-zA-Z0-9_-]+>', handler='handlers.wiki.EditPage', 
          name='wiki_page'),
    Route(r'/wiki/_history/<subject:[a-zA-Z0-9_-]+>', handler='handlers.wiki.History', 
          name='wiki_page'),
    ]

# main app
app = WSGIApplication(routes=routes, debug=True)
