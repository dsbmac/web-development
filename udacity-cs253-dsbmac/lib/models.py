"""
  Model for db storage objects
"""

from google.appengine.ext import db


# User model
class User(db.Model):
  username = db.StringProperty(required=True)
  created = db.DateTimeProperty(auto_now_add=True)
  email = db.StringProperty(required=False)
  pw_hash = db.StringProperty(required=False)