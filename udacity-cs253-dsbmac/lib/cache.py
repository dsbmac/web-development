"""
    Caching data
"""

import logging
import time
from google.appengine.api import memcache
from google.appengine.ext import db


# GLOBALS

# this dictionary key contains a key for every subject
# subject will contain another dictionary
# keys inside: current_ver (int)
#              history (list of int)


def get(key, subject=None):
    """
        call to this function and routed to subroutines based on key. Takes a 
        key str returns a cache tuple of format 
        (dbObject, timestamp, last queried object_id)
    """

    logging.info("cache.get..." + key + ', ' + subject)

    if key == 'wiki_single':
        wiki_id = get_wiki_id(subject)
        logging.info('wiki_id: ' + str(wiki_id))
        cache_data = check_cache(key, wiki_id)

        # extract and parse wiki and timestamp from the cache data object
        wiki = cache_data[0]
        time_last_update = cache_data[1]
        time_since = round(time.time() - time_last_update)

        return wiki, time_since

    return None


def check_cache(key, object_id):
    """
        stores a tuple value for the different apps.
        Takes a string key and int object id.
        ex. 'single_wiki', 1234566
        The tuple is in format (db obj, time last checked, obj id)
    """
    logging.info('check_cache...')

    # lookup the cache
    appValue = memcache.get(key)

    # check for a Wiki id to see if this is the same request for a Wiki
    prev_lookup = None
    if appValue:
        prev_lookup = appValue[2] 

    # hit the db query if no cache or requesting a different Wiki id
    if prev_lookup is None or (prev_lookup != object_id):
        logging.info("DB QUERY: " + str(object_id))
        dbkey = str(
            db.Key.from_path('WikiPage', int(object_id), parent=wiki_key()))
        dbObject = db.get(dbkey)

        appValue = (dbObject, time.time(), object_id)
        memcache.set(key, appValue)

    return appValue


def single_wiki(topic):

    # lookup the cache
    value = memcache.get(key)

    # check for a Wiki id to see if this is the same request for a Wiki
    cache_wiki_id = None
    if value:
        cache_wiki_id = value[2]

    # hit the db query if no cache or requesting a different Wiki id
    if cache_wiki_id is None or (cache_wiki_id != wiki_id):
        logging.error("DB QUERY")
        Wiki_key = str(
            db.Key.from_path('Wiki', int(wiki_id), parent=wiki_key()))
        Wiki = db.get(Wiki_key)

        value = (Wiki, time.time(), wiki_id)
        memcache.set(key, value)

    return value


def wiki_key(name='default'):
    """
      value of the wiki's parent for Google app data store
    """

    return db.Key.from_path('wikis', name)


def get_wiki_id2(subject):
    """
        gets the wiki obj id associated with that subject
    """

    pageQuery = db.GqlQuery(
        "SELECT * FROM WikiPage WHERE subject = :1", subject)
    wiki = pageQuery.get()
    if wiki:
        result = wiki.key().id()
        return result

    return None

    # wiki_ids.get(subject)


def get_wiki_id(subject, single=True):
    """
        takes a string subject and optional flag for history
        returns the wiki obj id associated with that subject
    """
    logging.info('get_wiki_id...' + subject)

    # retrive id dictionary from cache to lookup the id from subject key
    wiki_ids = memcache.get('wiki_ids')

    if wiki_ids:
        logging.info('wiki_ids: ' + str(len(wiki_ids)))
        if subject in wiki_ids:
            logging.info('subject found in dict')

            # single lookup
            if single:
                logging.info('looking up single id')
                subject_dict = wiki_ids[subject]

                return subject_dict['current_ver']

            else:
                return subject_dict['history']

    else:
        logging.error('no wiki_ids dict in memcache, initializing one...')
        memcache.set('wiki_ids', {})

        return []


def get_wikis(subject):
    """
        takes a string subject and returns a list of wikis db objects
    """

    wiki_ids = memcache.get('wiki_ids')

    result = []
    for wiki_id in wiki_ids:
        wkey = str(
            db.Key.from_path('WikiPage', int(wiki_id), parent=wiki_key()))
        wiki = db.get(wkey)
        result.append(wiki)

    return result


def set_wiki_id(subject, wiki_id):
    """
        sets the wiki subject to the wiki_id. returns void
    """
    logging.info("set_wiki_id...")
    key = 'wiki_ids'
    wiki_ids = memcache.get(key)

    # init the id dict
    if not wiki_ids:
        wiki_ids = {}

    # if not in the dict init a new one
    if subject not in wiki_ids:
        logging.info("adding " + subject)
        subject_dict = {}
        subject_dict['current_ver'] = None
        subject_dict['history'] = []
        wiki_ids[subject] = subject_dict

    # retrieves the dictionary object containing the db ids for the subject
    subject_dict = wiki_ids[subject]
    subject_dict['current_ver'] = wiki_id
    subject_dict['history'].append(wiki_id)
    wiki_ids[subject] = subject_dict

    memcache.set(key, wiki_ids)
