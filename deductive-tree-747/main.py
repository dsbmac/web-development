#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import os, re
from string import maketrans
from string import letters

MONTHS = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']

MONTHS_ABRREVIATED = dict((m[:3].lower(), m) for m in MONTHS)

escapeDict = {'>': '&gt;', '<': '&lt;', '"':  '&quot;', '&': '&amp;'}

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASS_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r'^[\S]+@[\S]+\.[\S]+$')


# FORMS

dob_form = """
  <form method="post">
    <label>
      Month
      <input name="month" value="{month}">
    </label>
    <label>
      Day
      <input name="day" value="{day}">
    </label>
    <label>
      Year
      <input name="year" value="{year}">
    </label>
    <br>
    <br>
    <input type="submit">
    <br>
    <div style="color:red">{error}</div>
  </form>
"""

rot13_form = """
<!DOCTYPE html>

<html>
  <head>
    <title>Lesson 2 Rot 13</title>
  </head>
  <body>
    <h2>Enter some text to ROT13:</h2>
    <form method="post">
      <textarea name="text" style="height: 100px; width: 400px;">{}</textarea>
      <br>
      <input type="submit">
    </form>
  </body>
</html>
"""

signup_form = """
<!DOCTYPE html>

<html>
  <head>
    <title>Lesson 2 Signup</title>
    <style type="text/css">
      .label {{text-align: right}}
      .error {{color: red}}
    </style>
  </head>

  <body>
    <h2>Signup</h2>
    <form method="post">
      <table>
        <tr>
          <td class="label">
            Username
          </td>
          <td>
            <input type="text" name="username" value="{username}">
          </td>
          <td class="error">
            {error_username}
          </td>
        </tr>
        <tr>
          <td class="label">
            Password
          </td>
          <td>
            <input type="password" name="password" value="">
          </td>
          <td class="error">
            {error_password}
          </td>
        </tr>
        <tr>
          <td class="label">
            Verify Password
          </td>
          <td>
            <input type="password" name="verify" value="">
          </td>
          <td class="error">
            {error_verify}
          </td>
        </tr>
        <tr>
          <td class="label">
            Email (optional)
          </td>
          <td>
            <input type="text" name="email" value="{email}">
          </td>
          <td class="error">
            {error_email}
          </td>
        </tr>
      </table>
      <br>
      <br>
      <input type="submit">
    </form>
  </body>
</html>
"""


class SignupHandler(webapp2.RequestHandler):
    """
        handles signup
    """

    def get(self):
        self.write_form()

    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        # validate data
        have_error, params = self.validate_signup(username, password, verify, 
                                                  email)

        if have_error:
            self.write_form(params)
        else:
            self.redirect('/lesson2/signup/welcome?username=' + username)

    def write_form(self, params=None):
        """
          creates a date of birth form to render on the page
          Takes nothing and returns a string.
        """

        # string sub dict
        if not params:
          params = {'username': '',
           'password': '',
           'verify': '',
           'email': '',
           'error_username': '',
           'error_password': '',
           'error_verify': '',
           'error_email': ''
            }

        params = escape_params(params)

        self.response.write(signup_form.format(**params))

    def validate_signup(self, username, password, verify, email):
        """
          validates signup input
        """

        params = {
         'username': username,
         'password': password,
         'verify': verify,
         'email': email,
         'error_username': '',
         'error_password': '',
         'error_verify': '',
         'error_email': ''
          }

        have_error = False

        # validate data
        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        return have_error, params



class WelcomeHandler(webapp2.RequestHandler):
    """
      handler for a correct signup submission

    """

    def get(self):
        username = self.request.get('username')
        msg = "Welcome, {}. Thank you for signing up."
        self.response.write(msg.format(username))


class Rot13Handler(webapp2.RequestHandler):

    """
        handles rot13
    """

    def get(self):
        self.write_form()

    def post(self):
        text = self.request.get('text')
        if text:
            text = encode_rot13(text)
            text = escape_html(text)
        self.write_form(text)

    def write_form(self, text=""):
        self.response.write(rot13_form.format(text))


class MainHandler(webapp2.RequestHandler):

    """
      Handler for main form page
    """

    def get(self):
        self.write_form()

    def post(self):
        """
        handles the form submission
        """

        month = self.request.get("month")
        day = self.request.get("day")
        year = self.request.get("year")

        # data validation
        error = self.create_dob_error_msg(month, day, year)
        if error:
            self.write_form(month, day, year, error)
        else:
            self.redirect('/thanks')

    def write_form(self, user_month="", user_day="", user_year="", error=""):
        """
          creates a date of birth form to render on the page
          Takes nothing and returns a string.
        """

        # string sub dict
        form_data = {
            'month': escape_html(user_month),
            'day': escape_html(user_day),
            'year': escape_html(user_year),
            'error': error
        }

        self.response.write(dob_form.format(**form_data))


    def create_dob_error_msg(self, user_month, user_day, user_year):
        """
          creates a customized error msg for the invalid for input
          takes string user input from the form and returns a string error msg.
          If there is no error returns empty string
        """

        user_month = valid_month(user_month)
        user_day = valid_day(user_day)
        user_year = valid_year(user_year)

        user_input = [user_month, user_day, user_year]
        if None in user_input:
            error = "The {} you entered is invalid"
            problems = ['month', 'day', 'year']
            return error.format(problems[user_input.index(None)])

        else:
            return ""


class ThanksHandler(webapp2.RequestHandler):

    """
      handler for a correct date submission

    """

    def get(self):
        self.response.write("Thank you for entering your date of birth.")


def escape_html(string):
    """
      replaces:
        > with &gt;
        < with &lt;
        " with &quot;
        & with &amp;
      and returns the escaped string
      Note that your browser will probably automatically 
      render your escaped text as the corresponding symbols, 
      but the grading script will still correctly evaluate it.
    """

    result = ''

    for c in string:
        if c in escapeDict:
            char = escapeDict[c]
        else:
            char = c
        result += char
    return result

def escape_params(params):
  for key, value in params.iteritems():
    params[key] = escape_html(value)

  return params

def valid_month(month):
    """
      Validates month entry. 
      Takes a string and returns a string or None

      ex.
      valid_month("january") #=> "January"    
      valid_month("January") #=> "January"
      valid_month("foo") #=> None
      valid_month("") #=> None

    """
    result = None

    if month:
        short_month = month[:3].lower()
        if short_month in MONTHS_ABRREVIATED:
            result = MONTHS_ABRREVIATED[short_month]

    return result


def valid_day(day):
    """

      verify whether the string a user enters is a valid 
      day. takes as input a String, and returns either a valid 
      Int or None. 

      If the passed in String is 
      not a valid day, return None. 
      If it is a valid day, then return 
      the day as an Int, not a String. Don't 
      worry about months of different length. 
      Assume a day is valid if it is a number 
      between 1 and 31.
      Be careful, the input can be any string 
      at all, you don't have any guarantees 
      that the user will input a sensible 
      day.

      ex.
      valid_day('0') => None    
      valid_day('1') => 1
      valid_day('15') => 15
      valid_day('500') => None  

    """

    if day.isdigit():
        n = int(day)
        if 1 <= n <= 31:
            return n
    return None


def valid_year(year):
    """
      Modify the valid_year() function to verify 
      whether the string a user enters is a valid 
      year. 

      If the passed in parameter 'year' 
      is not a valid year, return None. 
      If 'year' is a valid year, then return 
      the year as a number. Assume a year 
      is valid if it is a number between 1900 and 
      2020.

      ex.
      valid_year('0') => None    
      valid_year('-11') => None
      valid_year('1950') => 1950
      valid_year('2000') => 2000

    """

    if year.isdigit() and 1900 <= int(year) <= 2020:
        return int(year)
    return None


def valid_username(username):
    """
      validates the username
      takes a string and returns a Boolean
    """

    return username and USER_RE.match(username)


def valid_password(password):
    """
      validates the password
      takes a string and returns a Boolean
    """
    return password and PASS_RE.match(password)

def valid_email(email):
    """
      validates the email
      takes a string and returns a Boolean
    """  
    return not email or EMAIL_RE.match(email)


def encode_rot13(string):
    """
        takes a string and returns an encoded string
    """

    intab = "abcdefghijklmnopqrstuvwxyz"
    outtab = "nopqrstuvwxyzabcdefghijklm"
    trantab = maketrans(intab, outtab)

    # make the lower conversion
    string = str(string).translate(trantab)

    intab = "abcdefghijklmnopqrstuvwxyz".upper()
    outtab = "nopqrstuvwxyzabcdefghijklm".upper()
    trantab = maketrans(intab, outtab)

    # make the upper conversion
    return str(string).translate(trantab)


def create_error_msgs(user_input, error_fields):
    """
      creates a customized error msg for the invalid for input
      takes string user input from the form and returns a string error msg.
      If there is no error returns empty string
    """
    params = {}
    have_error = False

    for i, valid_entry in enumerate(user_input):
        error = ''

        if not valid_entry:
            error = "error"
            error.format(error_fields[i])
            have_error = True

        params[error_fields[i]] = error

    return have_error, params


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/thanks', ThanksHandler),
    ('/lesson2/rot13', Rot13Handler),
    ('/lesson2/signup', SignupHandler),
    ('/lesson2/signup/welcome', WelcomeHandler)
], debug=True)
