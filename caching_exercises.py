"""
  exercises in the caching unit
"""

import time

# complex_computation() simulates a slow function. time.sleep(n) causes the
# program to pause for n seconds. In real life, this might be a call to a
# database, or a request to another web service.


def complex_computation(a, b):
    time.sleep(.5)
    return a + b

cache = {}


def cached_computation(a, b):
    """
      Improve the cached_computation() function below so that it caches
      results after computing them for the first time so future calls are faster
    """

    key = (a, b)

    if key in cache:
        return cache[key]
    else:
        value = complex_computation(a, b)
        cache[key] = value
        return value

start_time = time.time()
print cached_computation(5, 3)
print "the first computation took %f seconds" % (time.time() - start_time)

start_time2 = time.time()
print cached_computation(5, 3)
print "the second computation took %f seconds" % (time.time() - start_time2)


# handy link
# http://stackoverflow.com/questions/423379/using-global-variables-in-a-function-other-than-the-one-that-created-them
# maybe a link to itertools

SERVERS = ['SERVER1', 'SERVER2', 'SERVER3', 'SERVER4']

# QUIZ - implement the function get_server, which returns one element from the
# list SERVERS in a round-robin fashion on each call. Note that you should
# comment out all your 'print get_server()' statements before submitting
# your code or the grading script may fail. For more info see:
# http://forums.udacity.com/cs253-april2012/questions/22327/unit6-13-quiz-problem-with-submission


def createGenerator():
    idx = 0
    while True:
        if idx >= len(SERVERS):
            idx = 0
        yield idx
        idx += 1

gen = createGenerator()


def get_server():
    idx = gen.next()
    return SERVERS[idx]

# better norvig solution
# get_server = itertools.cycle(SERVERS).next


# print get_server()
# print get_server()
# print get_server()
# print get_server()
# print get_server()
# print get_server()
# print get_server()
# print get_server()

# >>> SERVER1
# >>> SERVER2
# >>> SERVER3
# >>> SERVER4
# >>> SERVER1
# >>> SERVER2
# >>> SERVER3
# >>> SERVER4


# QUIZ implement the basic memcache functions

CACHE = {}


def set(key, value):
    """
        return True after setting the data
    """

    CACHE[key] = value
    return True


def get(key):
    """
      return the value for key. return None if key not valid.
    """

    return CACHE.get(key)


def delete(key):
    """
        delete key from the cache
    """
    if key in CACHE:
        CACHE.pop(key, None)


def flush():
    """
        clear the entire cache
    """

    CACHE.clear()

# print set('x', 1)
#>>> True

# print get('x')
#>>> 1

# print get('y')
#>>> None

# delete('x')
# print get('x')
#>>> None


CACHE = {}

# return True after setting the data


def set(key, value):
    CACHE[key] = value
    return True

# return the value for key


def get(key):
    return CACHE.get(key)

# delete key from the cache


def delete(key):
    if key in CACHE:
        del CACHE[key]

# clear the entire cache


def flush():
    CACHE.clear()

# QUIZ - implement gets() and cas() below
# return a tuple of (value, h), where h is hash of the value. a simple hash
# we can use here is hash(repr(val))


def gets(key):
    val = CACHE.get(key)
    if val:
        hash_val = hash(repr(val))
        return val, hash_val

    # val is None
    return val


# set key = value and return True if cas_unique matches the hash of the value
# already in the cache. if cas_unique does not match the hash of the value in
# the cache, don't set anything and return False.
def cas(key, value, cas_unique):
    test = cas_unique == gets(key)[1]
    if test:
        CACHE[key] = value

    return test

# print set('x', 1)
# >>> True

# print get('x')
# >>> 1

# print get('y')
# >>> None

# delete('x')
# print get('x')
# >>> None

# set('x', 2)
# print gets('x')
# >>> 2, HASH

# print cas('x', 3, 0)
# >>> False
# #
# print cas('x', 4, 'HASH')
# >>> True
# #
# print get('x')
# >>> 4
